#include <stdio.h>
#include <stdbool.h>
#include "edth.h"

void setActiveWindow (int appData[], int aw)
{
	appData[3]=aw;
}

void iniWindow (int appData[], char windows[][appData[1]][appData[2]], int window)// funciona
{
	iniEmptyMatrix(appData[1], appData[2], windows[window]);
	//printMatrix(appData[1], appData[2], windows[window]);	//comprobacion
}

void iniWindows (int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2])//funciona
{
	for(int x=0; x<appData[0]; x++)
	{

		iniWindow (appData,  windows, x);
                appCursors[x][1]=0;	
		appCursors[x][0]=0;
	
	}
}

void printWindowInfo (int appData[], int appCursors[][2] ) //funciona
{
	printf("%d-%d,%d\n", appData[3], appCursors[appData[3]][0], appCursors[appData[3]][1]);
}

void printWindow (int appData[], char matrix[appData[1]][appData[2]])// funciona!!!!!!
{
	printf("    ");
	for(int i=0; i<appData[2]; i++)
	{
		printf("-");
	}
	printf("\n");
	printMatrix (appData[1], appData[2], matrix);
	printf("    ");
	for(int y=0; y<appData[2]; y++)
	{
		printf("-");
	}
	printf("\n");
}

void printCurrentWindow (int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2])//funciona
{
	printWindowInfo (appData, appCursors);
	printWindow (appData, windows[appData[3]]);
}

bool isLegalFile (int appData[], int y)//funciona
{
	return (appData[1]>y);
}

bool isLegalColumn (int appData[], int x)//funciona
{
	return (appData[2]>x);
}

void setYCursorOnWindow (int appData[], int appCursors[][2], int y)//funciona
{
	if(isLegalFile (appData, y))
	{
		appCursors[appData[3]][0]=y;
	}
	else{}
}

void setXCursorOnWindow (int appData[], int appCursors[][2], int x)//funciona
{
	if(isLegalColumn (appData, x))
	{
		appCursors[appData[3]][1]=x;
	}
	else{}
}

void insertCharOnWindow (int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char c)
{
	insertCharOnMatrix (appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][0], appCursors[appData[3]][1], c);
}

void deleteCurrentPositionOnWindow (int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2])
{
	deleteCharOnMatrix (appData[1], appData[2], windows[appData[3]], appCursors[appData[3]][0], appCursors[appData[3]][1]);
	//shiftRightArray(appData[2], windows[appData[3]], appCursors[appData[3]][1]);
}

int searchCharOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char c)
{
	searchCharOnMatrix (appData[1], appData[2], windows[appData[1]][appData[2]], appCursors[appData[3]][0], appCursors[appData[3]][1], c);
}

int searchNoCharOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char c)
{
	searchNoCharOnMatrix (appData[1], appData[2], windows[appData[1]][appData[2]], appCursors[appData[3]][0], appCursors[appData[3]][1], c);
}
