main: main.o arrays.o matrices.o ventanas.o parser.o 
	gcc -o main main.o arrays.o matrices.o ventanas.o parser.o
main.o: main.c edth.h
	gcc -c main.c
arrays.o: arrays.c edth.h
	gcc -c arrays.c
matrices.o: matrices.c edth.h
	gcc -c matrices.c
ventanas.o: ventanas.c edth.h
	gcc -c ventanas.c
parser.o: parser.c edth.h
	gcc -c parser.c
test:
	@echo "Comprobando casos"
	@echo "***************************************************"
	@echo "Prueba 1"
	@./main "jjjklllhiEsto es un texto" > salida.txt
	@diff -c salida.txt prueba1.txt
	@echo "Correcto"
	@echo "***************************************************"
	@echo "Prueba 2"
	@./main "ttTiEscribo&jjllliTexto&g" > salida.txt
	@diff -c salida.txt prueba2.txt
	@echo "Correcto" 
	@echo "***************************************************"
	@echo "Prueba 3"
	@./main "GiSon varias palabras&hhxxial&hhhx" > salida.txt
	@diff -c salida.txt prueba3.txt
	@echo "Correcto"
	@echo "***************************************************"
	@echo "Prueba 4"
	@./main "jjia b c d e f g h&hhhhwwiz" > salida.txt
	@diff -c salida.txt prueba4.txt
	@echo "Correcto"
	@echo "***************************************************"
	@echo "Prueba 5"
	@./main "ooOiLinea palabra&jLinea&DkhhhW" > salida.txt
	@diff -c salida.txt prueba5.txt
	@echo "Correcto"
	@echo "***************************************************"
	@echo "Test realizado"
