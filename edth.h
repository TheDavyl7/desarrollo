#include<stdio.h>
#include<stdbool.h>

void iniEmptyArray(int columns, char array[]);
void printArray(int columns, char array[]);
void putCharOnArray(int columns, char array[], int column, char c);
void shiftRightArray(int columns, char array[], int column);
void shiftLeftArray(int columns, char array[], int column);
void insertCharOnArray(int columns, char array[], int column, char c);

void iniEmptyMatrix(int files, int columns, char matrix[files][columns]);
void printMatrix(int files, int columns, char matrix[files][columns]);
void insertCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column, char c);
void deleteCharOnMatrix(int files, int columns, char matrix[files][columns], int file, int column);

void setActiveWindow(int appData[], int aw);
void iniWindow(int appData[], char windows[][appData[1]][appData[2]], int window);
void iniWindows(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]);
void printWindowInfo(int appData[], int appCursors[][2]);
void printWindow(int appData[], char matrix[appData[1]][appData[2]]);
void printCurrentWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]);
bool isLegalFile(int appData[], int y);
void setYCursorOnWindow(int appData[], int appCursors[][2], int y);
bool isLegalColumn(int appData[], int x);
void setXCursorOnWindow(int appData[], int appCursors[][2], int x);
void insertCharOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char c);
void deleteCurrentPositionOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]);
void gotoBeginLine (int appData[], int appCursors[][2]);
void deleteCurrentFile (int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]);
void gotoFirstFile (int appData[], int appCursors[][2]);
void gotoLastFile (int appData[], int appCursors[][2]);
void moveCursorRight (int appData[], int appCursors[][2]);
void moveCursorDown (int appData[], int appCursors[][2]);
void moveCursorUp (int appData[], int appCursors[][2]);
void moveCursorLeft (int appData[], int appCursors[][2]);
void parserInput (int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char input);
void changeNextWindow(int appData[]);
void changePreviousWindow(int appData[]);
void gotoLastLine (int appData[], int appCursors[][2]);
void nextWord(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]);
int searchCharOnArray (int columns, char array[], int column, char c);
int searchNoCharOnArray (int columns, char array[], int column, char c);
int searchCharOnMatrix (int files, int columns, char matrix[files][columns], int file, int column, char c);
int searchNoCharOnMatrix (int files, int columns, char matrix[files][columns], int file, int column, char c);
int searchCharOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char c);
int searchNoCharOnWindow(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char c);
void shiftDown(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]);
void shiftDownMatrix(int files, int columns, char matrix[files][columns], int file, int column);
