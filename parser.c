#include <stdio.h>
#include <stdbool.h>
#include "edth.h"

void gotoBeginLine (int appData[], int appCursors[][2])//funciona
{
	//appCursors[appData[3]][1]=0;	
	setXCursorOnWindow (appData, appCursors, 0);
}

void gotoLastLine (int appData[], int appCursors[][2])//funciona
{
	setXCursorOnWindow (appData, appCursors, appData[2]);
}

void deleteCurrentFile (int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2])
{
	for(int x=1; x<=appData[2]; x++)
	{
		setYCursorOnWindow (appData, appCursors, x);
		deleteCurrentPositionOnWindow (appData, windows, appCursors);
	}
}

void gotoFirstFile (int appData[], int appCursors[][2])//funciona
{
	setYCursorOnWindow (appData, appCursors, 0);
}

void gotoLastFile (int appData[], int appCursors[][2])//funciona
{
	setYCursorOnWindow (appData, appCursors, 4);
}

void moveCursorRight (int appData[], int appCursors[][2])//funciona
{
	setXCursorOnWindow (appData, appCursors, appCursors[appData[3]][1]+1);
}

void moveCursorDown (int appData[], int appCursors[][2])//funciona
{
	setYCursorOnWindow (appData, appCursors, appCursors[appData[3]][0]+1);
}

void moveCursorUp (int appData[], int appCursors[][2])//funciona
{
	setYCursorOnWindow (appData, appCursors, appCursors[appData[3]][0]-1);
}

void moveCursorLeft (int appData[], int appCursors[][2])//funciona
{
	setXCursorOnWindow (appData, appCursors, appCursors[appData[3]][1]-1);
}

void changeNextWindow(int appData[])//funciona
{
	if(appData[3]==0)
	{
		setActiveWindow (appData, 1);
	}
	else if(appData[3]==1)
	{
		setActiveWindow (appData, 2);
	}
	else if(appData[3]==2)
	{
		setActiveWindow (appData, 0);
	}
}

void changePreviousWindow(int appData[])//funciona
{
	if(appData[3]==0)
	{
		setActiveWindow (appData, 2);
	}
	else if(appData[3]==1)
	{
		setActiveWindow (appData, 0);
	}
	else if(appData[3]==2)
	{
		setActiveWindow (appData, 1);
	}
}

void nextWord(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2]) //funciona
{
	int posicion= searchCharOnWindow(appData, windows, appCursors, ' ');
	if(posicion<appData[2])
	{
		int appCursorsTemp[appData[0]][2];
		appCursorsTemp[appData[3]][0]=appCursors[appData[3]][0];
		appCursorsTemp[appData[3]][1]=posicion;
		posicion= searchCharOnWindow(appData, windows, appCursorsTemp, ' ');
		if(posicion<appData[2])
		{
			appCursors[appData[3]][1]= posicion+1;
		}
	}
}

void shiftDown(int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2])//arreglar
{
	shiftDownMatrix(appData[1], appData[2], windows, appCursors[appData[3]][0], appCursors[appData[3]][1]);
}

void parserInput (int appData[], char windows[][appData[1]][appData[2]], int appCursors[][2], char input)
{
	static int mode=0;
	if(mode == 0)
	{
		switch(input)
		{
			case 'k':
				moveCursorUp(appData, appCursors);
				break;
			
			case 'j':
				moveCursorDown(appData, appCursors);
				break;

			case 'h':
				moveCursorLeft(appData, appCursors);
				break;

			case 'l':
				moveCursorRight(appData, appCursors);
				break;

			case 't':
				changeNextWindow(appData);
				break;

			case 'T':
				changePreviousWindow(appData);
				break;

			case 'g':
				gotoFirstFile(appData, appCursors);
				break;

			case 'G':
				gotoLastFile(appData, appCursors);
				break;

			case 'w':
				nextWord(appData, windows, appCursors);
				break;

			case 'x':
				deleteCurrentPositionOnWindow (appData, windows, appCursors);
				break;

			case 'o':
				moveCursorDown (appData, appCursors);
				shiftDown(appData, windows, appCursors);
				moveCursorUp (appData, appCursors);
				gotoBeginLine (appData, appCursors);
				mode=1;
				break;

			case 'i':
				mode=1;
				break;
		}
	}
	else if(mode == 1)
	{
		switch(input)
		{
			case '&':
				mode=0;
				break;
			default:
				insertCharOnWindow(appData, windows, appCursors, input);
				moveCursorRight(appData, appCursors);
		}
	}
}

