#include <stdio.h>
#include <stdbool.h>
#include "edth.h"
void iniEmptyArray (int columns, char array[])//funciona
{
	for(int i=0; i<columns; i++)
	{
		array[i]=' ';
	}
}

void printArray (int columns, char array[])//funciona
{
	for(int i=0; i<columns; i++)
	{
		printf("%c", array[i]);
	}
	//printf("a");//comprobar
}

void putCharOnArray (int columns, char array[], int column, char c)//funciona
{
	// a--c--f---
	array[column]=c;
}

void shiftRightArray (int columns, char array[], int column)//funciona
{
	for(int i = columns-2; i>=column; i--)
	{
		array[i+1]= array[i];
	}
	array[column]=' ';
}
		

void shiftLeftArray (int columns, char array[], int column)//funciona
{
	for(int i = column; i<columns-1; i++)
	{
		array[i-1]= array[i];
	}
	array[columns]=' ';
}

void insertCharOnArray (int columns, char array[], int column, char c)//funciona
{
	if (column==9)
	{
		putCharOnArray(columns, array, column, c);
	}
	else
	{
		shiftRightArray(columns, array, column);
		putCharOnArray(columns, array, column, c);
	}
	//shiftRightArray(columns, array, column);
	//array[column]=c;
}

int searchCharOnArray (int columns, char array[], int column, char c)
{
	for (int i=column; i<columns; i++)
	{
		if(array[i]==c)
		{
			return i;
		}
	}
}

int searchNoCharOnArray (int columns, char array[], int column, char c)
{
	for (int i=column; i<columns; i++)
	{
		if(array[i]!=c)
		{
			return i;
		}
	}
}
